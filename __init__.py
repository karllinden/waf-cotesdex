#
# Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import os
import re

from waflib import Configure, Errors, Node, Task, TaskGen, Utils

@Configure.conf
def find_program_once(self, filename, **kw):
    """
    Find a program using find_program, but do not re-check for the
    program if it has already been checked.

    :param filename: the name of the program to check for
    :type filename: str
    """
    filename = Utils.to_list(filename)

    var = kw.get('var', re.sub(r'[-.]', '_', filename[0].upper()))
    if self.env[var] or self.env[var] == False:
        # A check has already been done. Do not re-check.
        return

    try:
        self.find_program(filename, **kw)
    except Errors.ConfigurationError:
        self.env[var] = False
        raise

def options(opt):
    opt.load('autooptions waf_unit_test')

    options = [
        (
            'doc',
            'Build documentation',
            ['asciidoc', 'cotesdex']
        ),
        (
            'examples',
            'Build and install examples',
            ['cotesdex']
        ),
        (
            'tests',
            'Run tests',
            ['bash', 'cotesdex']
        )
    ]
    for (o, h, fns) in options:
        if not hasattr(opt, 'cotesdex_no_%s_option' % o):
            option = opt.add_auto_option(o, h)
            for fn in fns:
                option.add_function(find_program_once, fn)

def configure(conf):
    conf.load('autooptions waf_unit_test')

    # Respect the environment's COTESDEXFLAGS.
    try:
        conf.env['COTESDEXFLAGS'] = os.environ['COTESDEXFLAGS']
    except KeyError:
        pass

@TaskGen.feature('no_process_source')
@TaskGen.before('process_source')
@TaskGen.taskgen_method
def no_process_source(self):
    try:
        self.meths.remove('process_source')
    except ValueError:
        pass

@TaskGen.taskgen_method
def get_in_nodes(self):
    """
    Get the in nodes for a TaskGen.

    To nodes cannot be used here because it looks for nodes in the build
    directory.
    """
    find = self.path.get_src().find_node

    if isinstance(self.source, list):
        li = self.source
    else:
        li = [self.source]

    nodes = []
    for x in Utils.to_list(li):
        if isinstance(x, str):
            node = find(x)
        else:
            node = x
        if not node:
            err = 'source not found: {} in {}'.format(x, self)
            raise Errors.WafError(err)
        nodes.append(node)

    return nodes

@TaskGen.feature('cotesdex')
@TaskGen.feature('cotesdex_doc')
@TaskGen.before('process_source')
def cotesdex_doc(self):
    self.no_process_source()

    if not self.env['DOC']:
        return

    install_path = getattr(
            self,
            'install_path',
            self.env['HTMLDIR'] or self.env['DOCDIR'])

    in_nodes = self.get_in_nodes()
    for in_node in in_nodes:
        txt_node = in_node.change_ext('.txt')
        out_node = in_node.change_ext('.html')
        self.bld(
            features = 'no_process_source',
            source   = in_node,
            target   = txt_node,
            rule     = '${COTESDEX} ${COTESDEXFLAGS} ' +
                       '--asciidoc -o ${TGT} ${SRC}'
        )
        self.bld(
            source       = txt_node,
            target       = out_node,
            rule         = '${ASCIIDOC} -o ${TGT} ${SRC}',
            install_path = install_path
        )

@TaskGen.feature('cotesdex')
@TaskGen.feature('cotesdex_example')
@TaskGen.before('process_source')
def cotesdex_example(self):
    self.no_process_source()

    if not self.env['EXAMPLES']:
        return

    install_path = getattr(
            self,
            'install_path',
            self.env['EXAMPLESDIR'] or self.env['DOCDIR'])

    in_nodes = self.get_in_nodes()
    for in_node in in_nodes:
        out_node = in_node.get_bld()
        self.bld(
            features     = 'no_process_source',
            source       = in_node,
            target       = out_node,
            rule         = '${COTESDEX} ${COTESDEXFLAGS} ' +
                           '--strip -o ${TGT} ${SRC}',
            install_path = install_path
        )

@TaskGen.feature('cotesdex')
@TaskGen.feature('cotesdex_test')
@TaskGen.before('process_source')
def cotesdex_test(self):
    self.no_process_source()

    if not self.env['TESTS']:
        return

    in_nodes = self.get_in_nodes()
    for in_node in in_nodes:
        exe_node = False
        if in_node.abspath().endswith('.c'):
            exe_node = in_node.change_ext('')

            # Create a new TaskGen for the compiled test. Clone should
            # be used here because this TaskGen can contain things such
            # as use, define, and include.
            tg = self.clone(self.env)

            # Make sure that no infinite recursion is created.
            try:
                tg.features.remove('cotesdex')
            except ValueError:
                pass
            for x in ['doc', 'example', 'test']:
                y = 'cotesdex_' + x
                for l in [tg.features, tg.meths]:
                    try:
                        l.remove(y)
                    except ValueError:
                        pass

            # Finally, make the created TaskGen generate the compiled
            # test.
            tg.features.extend(['c', 'cprogram'])
            tg.meths.append('process_source')
            tg.source = [in_node]
            tg.target = [exe_node]
            tg.install_path = None

        # Construct the rule that generates the test script.
        rule = '${COTESDEX} ${COTESDEXFLAGS} --test '

        # Handle the --executable argument.
        executable = getattr(self, 'cotesdex_executable', None)
        if executable:
            if isinstance(executable, Node.Node):
                exe_node = executable
            else:
                exe_node = self.bld.path.find_or_declare(executable)
            rule += '--executable=' + exe_node.abspath() + ' '

        # Handle the --here-string argument.
        here_string = getattr(self, 'cotesdex_here_string', None)
        if here_string:
            rule += '--here-string=' + here_string + ' '

        rule += '-o ${TGT} ${SRC[0]}'

        # Manual dependencies can be added through the deps variable.
        deps = getattr(self, 'cotesdex_deps', [])
        deps = Utils.to_list(deps)

        # Let the shell script depend on the execuable node as well,
        # since the test script task can only depend on the test script.
        sh_node = in_node.change_ext('.sh')
        source = [in_node, exe_node]
        source.extend(deps)
        self.bld(
            source      = source,
            features    = 'no_process_source',
            target      = sh_node,
            rule        = rule,
            intall_path = None
        )

        self.bld(
            features              = 'no_process_source test_scripts',
            test_scripts_source   = sh_node,
            test_scripts_template = '${BASH} ${SCRIPT}'
        )
